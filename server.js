const express = require('express');
const body_parser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;
const URL_BASE = '/techu/v1/';
const URL_BASE_LOGIN = '/apitechu/v1/';
const usersFile = require('./user.json');

app.listen(port, function(){
  console.log("nodejs listening" + port);
})

app.use(body_parser.json());


app.get(URL_BASE + 'users',
  function(req, res){
    res.status(200).send(usersFile);
});

app.get(URL_BASE + 'users',
  function(req, res) {
    console.log("GET con query string.");
    console.log(req.query.id);
    console.log(req.query.country);
    res.send(usersFile[pos - 1]);
    respuesta.send({"msg" : "GET con query string"});
});

app.get(URL_BASE + 'users/:id',
  function(req, res){
    console.log(req.params.id);
    let pos = req.params.id - 1;
    let respuesta =
      (userFile[pos] == undefined) ? {"msg": "usuario no existente"} : userFile[pos];
    let status_code = (userFile[pos] == undefined) ? 404 : 200;
    response.send(respuesta);
  }
);

app.post(URL_BASE + 'users',
function(req, res){
  console.log('POST a users');
  let tam = usersFile.length;
  let new_user = {
    "id": tam + 1,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email_name": req.body.email,
    "password": req.body.password
  }
  console.log(new_user);
  usersFile.push(new_user);
  res.send({"msg": "usuario creado"})
}
);

app.put(URL_BASE + 'users/:id',
  function(req, res){
  let idBuscar = req.params.id;
  let updateUser = req.body;
  for(i = 0; i < usersFile.length; i++){
    console.log(usersFile[i].id);
    if(usersFile[i].id == idBuscar) {
      usersFile[i] = updateUser;
      res.send({"msg": "usuario actualizado Correctamente.", updateUser})
    }
  }
  res.send({"msg": "Usuario no encontrado.", updateUser});
});

app.delete(URL_BASE + 'users/:id',
  function(req, res){
    console.log('Delete a users');
    usersFile.splice(req.params.id-1, 1);
    res.send({"msg": "usuario eliminado correctamente"});
});


app.post(URL_BASE_LOGIN + 'login',
  function(request, response) {
    console.log("POST /apitechu/v1/login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;

    var logged = false;
    var id;

    for(us of usersFile) {
      console.log(us);
      if(us.email == user && us.password == pass) {
        logged = true;
        us.logged = true;
        id = us.id;
        writeUserDataToFile(usersFile);
      }
    }

    if(logged){
      console.log("Login correcto!");
      response.send({"msg" : "Login correcto.", "idUsuario" : id});
    } else {
      console.log("Login incorrecto.");
      response.send({"msg" : "Login incorrecto."});
    }

});

// LOGOUT - users.json
app.post(URL_BASE_LOGIN + 'logout',
  function(request, response) {
    console.log("POST /apitechu/v1/logout");
    var userId = request.body.id;
    var logged = true;
    var id;
    for(us of usersFile) {
      if(us.id == userId && us.logged) {
          delete us.logged; // borramos propiedad 'logged'
          writeUserDataToFile(usersFile);
          logged = false;
          id = us.id;
      }
    }

    if(!logged){
      console.log("Logout correcto!");
      response.send({"msg" : "Logout correcto.", "idUsuario" : id});
    } else {
      console.log("Logout incorrecto.");
      response.send({"msg" : "Logout incorrecto."});
    }

});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   }); }
